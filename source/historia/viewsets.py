from rest_framework import viewsets, status
from rest_framework.decorators import action, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from .serializers import HistoriaSerializer
from .models import Historia
from categoria.models import Categoria, TipoCategoria

from autor.models import Autor



class HistoriaViewsets(viewsets.ModelViewSet):
    queryset = Historia.objects.all()
    serializer_class = HistoriaSerializer

    @action(detail=False, methods=["POST"])
    @permission_classes(AllowAny)
    def get_all_autor(self, request):
        autor = Autor.objects.filter(pk=request.data["autor_pk"]).first()
        if not autor:
            return Response(data=f"Não existe este autor de chave {autor}.", status=status.HTTP_404_NOT_FOUND)
        historias = Historia.objects.filter(autor=autor)
        if not historias.exists():
            return Response(data="Não existem histórias deste autor", status=status.HTTP_404_NOT_FOUND)
        else:
            historias_serializedas = self.get_serializer(historias, many=True)
            return Response(data=historias_serializedas.data, status=status.HTTP_200_OK)

