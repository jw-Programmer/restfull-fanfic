from django.db import models
from django.conf import settings
from categoria.models import Categoria
from genero.models import Genero

from datetime import date
# Create your models here.
'''A classe história é a que mais possue informações e chaves em relação as demais classes. É bem complexa'''
class Historia(models.Model):
    titulo = models.CharField(max_length = 500)
    descricao = models.TextField(verbose_name="Descrição da história", max_length=8000)
    data_publicacao = models.DateField(auto_now=True)
    autor = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    categoria = models.ManyToManyField(Categoria)
    genero = models.ManyToManyField(Genero)

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = "História"
        verbose_name_plural = "Histórias"
