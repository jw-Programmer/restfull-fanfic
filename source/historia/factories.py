from django.contrib.auth import get_user_model
from model_mommy import mommy
from autor.factories import make_autor
from categoria.factories import make_categoria
from genero.factories import make_genero
from .models import Historia


def make_historia(**kargs):
    if "autor" not in kargs.keys():
        kargs["autor"] = make_autor()
    if "categoria" not in kargs.keys():
        kargs["categoria"] = make_categoria()
    if "genero" not in kargs.keys():
        kargs["genero"] = make_genero()
    return mommy.make(Historia)
