from django.db import models
from django.conf import settings 
# Create your models here.
'''app que permite a troca de mensagems'''
class Mensagem(models.Model):
    mensagem = models.TextField(max_length=2000)
    remetente = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="remetente_mensagem")
    destinatario = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="destinatario_mensagem")

    def __str__(self):
        return "Mensagem de "+ str(self.remetente) +" , para " + str(self.destinatario)
