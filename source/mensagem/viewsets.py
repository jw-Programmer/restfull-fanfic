from rest_framework import viewsets

from .models import Mensagem
from .serializers import MensagemSerializer

class MensagemViewset(viewsets.ModelViewSet):
    queryset = Mensagem.objects.all()
    serializer_class = MensagemSerializer