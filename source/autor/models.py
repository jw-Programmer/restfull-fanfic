from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.
'''Esta classe de autor irá extender  classe AbstrachUser para evitar maiores confusões com duas classes e pode
utilizar da autentificação'''
class Autor(AbstractUser):
    idade = models.IntegerField(default=18)
    descricao = models.TextField(max_length=8000, default="Este é uma descrição padrão para fins de testes. Por favaro, alterar")

    class Meta:
        verbose_name = "Autor"
        verbose_name_plural = "Autores"