from model_mommy import mommy
from django.contrib.auth import get_user_model

User = get_user_model()

def make_autor(**kargs):
    return mommy.make(User)