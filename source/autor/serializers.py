from rest_framework import serializers
from django.conf import settings
from .models import Autor
class AutorSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = Autor
        fields = ('id','username','email','name','idade','descricao','date_joined')

    def get_name(self, obj):
        return obj.get_full_name()

