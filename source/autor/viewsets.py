from rest_framework import viewsets
from .serializers import AutorSerializer
from .models import Autor

from historia.models import Historia
from historia.serializers import HistoriaSerializer

from rest_framework.decorators import action
from rest_framework.response import Response
class AutorViewset(viewsets.ModelViewSet):
    queryset = Autor.objects.all()
    serializer_class = AutorSerializer