from django.contrib import admin
from .models import Capitulo, Comentario
# Register your models here.
admin.site.register(Capitulo)
admin.site.register(Comentario)