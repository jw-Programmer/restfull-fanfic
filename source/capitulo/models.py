from django.db import models
from django.conf import settings

from datetime import date

from historia.models import Historia

# Create your models here.
'''Esta classe de comentário é para poder comentar nos capítulos'''

class Capitulo(models.Model):
    index = models.IntegerField()
    titulo = models.CharField(max_length = 50)
    texto = models.TextField(verbose_name="Texto do capitulo", max_length=8000)
    data_publicacao = models.DateField( auto_now=True)
    historia = models.ForeignKey(Historia, on_delete=models.CASCADE)

    def __str__(self):
        return "Capitulo "+str(self.index)+ ": "+self.titulo

    class Meta():
        verbose_name = "Capítulo"
        verbose_name_plural = "Capítulos"
    
class Comentario(models.Model):
    comentario = models.TextField(verbose_name="Comentário", max_length=5000)
    autor = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    capitulo = models.ForeignKey(Capitulo, on_delete=models.CASCADE)

    def __str__(self):
        return "Comentario de "+ str(self.autor) +" em "+str(self.capitulo)

    class Meta():
        verbose_name = "Comentário"
        verbose_name_plural = "Comentários"
