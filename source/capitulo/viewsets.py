from rest_framework import viewsets, status
from rest_framework.decorators import action, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from .serializers import CapituloSerilizer, ComentarioSerilizer
from .models import Capitulo, Comentario

from historia.models import Historia



class CapituloViewset(viewsets.ModelViewSet):
    queryset = Capitulo.objects.all()
    serializer_class = CapituloSerilizer

    @action(detail=False, methods=['post'])
    @permission_classes(AllowAny)
    def get_historia_capitulo(self, request):
        historia = Historia.objects.filter(pk=request.data["historia_pk"]).first()
        if not historia:
            return Response(data=f"Não existe este historia de chave {historia}", status=status.HTTP_404_NOT_FOUND)
        capitulos = Capitulo.objects.filter(historia=historia)
        if not capitulos.exists():
            return Response(data="Não existem capitulos na história", status=status.HTTP_404_NOT_FOUND)
        else:
            capitulos_serializados = self.get_serializer(capitulos, many=True)
            return Response(data=capitulos_serializados.data, status=status.HTTP_200_OK)


class ComentarioViewset(viewsets.ModelViewSet):
    queryset = Comentario.objects.all()
    serializer_class = ComentarioSerilizer

    @action(detail=False, methods=['post'])
    @permission_classes(AllowAny)
    def get_comantario_capitulo(self, request):
        capitulo = Capitulo.objects.filter(pk=request.data["capitulo_pk"]).first()
        if not capitulo:
            return Response(data=f"Não existe este capitulo de chave {capitulo}", status=status.HTTP_404_NOT_FOUND)
        comentarios = Comentario.objects.filter(capitulo=capitulo)
        if not comentarios.exists():
            return Response(data="Não existem comenatarios no capitulos", status=status.HTTP_404_NOT_FOUND)
        else:
            comentarios_serializados = self.get_serializer(
                comentarios, many=True)
            return Response(data=comentarios_serializados.data, status=status.HTTP_200_OK)
