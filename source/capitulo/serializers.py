from rest_framework import serializers
from .models import Capitulo, Comentario

class CapituloSerilizer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Capitulo
        fields = "__all__"

class ComentarioSerilizer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Comentario
        fields = "__all__"