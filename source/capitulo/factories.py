from model_mommy import mommy
from historia.factories import make_historia
from autor.factories import make_autor
from .models import Capitulo, Comentario

def make_capitulo(**kargs):
    if "historia" not in kargs.keys():
        kargs["historia"] = make_historia()
    return mommy.make(Capitulo)

def make_comentario(**kargs):
    if "capitulo" not in kargs.keys():
        kargs["capitulo"] = make_capitulo()
    if "autor" not in kargs.keys():
        kargs["autor"] = make_autor()
    return mommy.make(Comentario)
