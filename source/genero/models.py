from django.db import models

# Create your models here.
'''Este aqui são os gẽneros'''
class Genero(models.Model):
    nome = models.CharField(max_length=100)

    def __str__(self):
        return self.nome

    class Meta():
        verbose_name = "Genero"
        verbose_name_plural = "Generos"