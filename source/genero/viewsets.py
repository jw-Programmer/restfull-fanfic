from rest_framework import viewsets
from .serializers import GeneroSerializer
from .models import Genero

class GeneroViewset(viewsets.ModelViewSet):
    queryset = Genero.objects.all()
    serializer_class = GeneroSerializer