from model_mommy import mommy
from .models import Genero

def make_genero(**kargs):
    return mommy.make(Genero)