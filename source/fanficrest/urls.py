"""fanficrest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from autor.viewsets import AutorViewset
from categoria.viewsets import CategoriaViewset, TipoCategoriaViewset
from genero.viewsets import GeneroViewset
from historia.viewsets import HistoriaViewsets
from capitulo.viewsets import CapituloViewset, ComentarioViewset
from mensagem.viewsets import MensagemViewset

router = DefaultRouter()
router.register(r'autores', AutorViewset)
router.register(r'tipos-categoria', TipoCategoriaViewset)
router.register(r'categorias', CategoriaViewset)
router.register(r'generos', GeneroViewset)
router.register(r'historias', HistoriaViewsets)
router.register(r'capitulos', CapituloViewset)
router.register(r'comentarios', ComentarioViewset)
router.register(r'mensagens',MensagemViewset)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
