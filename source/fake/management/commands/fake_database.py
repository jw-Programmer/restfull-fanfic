from random import choice
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

from categoria.factories import make_tipo_categoria, make_categoria
from genero.factories import make_genero
from autor.factories import make_autor
from historia.factories import make_historia
from capitulo.factories import make_capitulo, make_comentario

User = get_user_model()
class Command(BaseCommand):
    help = "Commando para popular o banco"

    def handle(self, *args, **options):
        #criar 10 tipos de categorias
        tipos_categorias=[]
        for i in range(1,10):
            tipo = make_tipo_categoria()
            tipos_categorias.append(tipo)
        categorias=[]
        for i in range(1,20):
            categoria = make_categoria(tipo_categoria=choice(tipos_categorias))
            categorias.append(categoria)
        generos=[]
        for i in range(1,10):
            genero = make_genero()
            generos.append(genero)
        # 5 autores
        autores = []
        for i in  range(1,5):
            autor = make_autor()
            autores.append(autor)
        # 2 histórias por autor
        historias = []
        for autor in autores:
            historia = make_historia(autor=autor, categoria=choice(categorias),genero=choice(generos))
            historias.append(historia)
        capitulos = []
        for historia in historias:
            for i in range(1,5):
                capitulo = make_capitulo(historia=historia)
                capitulos.append(capitulo)
        comentarios = []
        for capitulo in capitulos:
            comentario = make_comentario(capitulo=capitulo, autor=choice(autores))
            comentarios.append(comentario)

        