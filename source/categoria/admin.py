from django.contrib import admin
from .models import Categoria, TipoCategoria
# Register your models here.
admin.site.register(TipoCategoria)
admin.site.register(Categoria)