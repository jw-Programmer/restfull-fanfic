from model_mommy import mommy
from .models import TipoCategoria, Categoria

def make_tipo_categoria(**kargs):
    return mommy.make(TipoCategoria)

def make_categoria(**kargs):
    if "tipo_categoria" not in kargs.keys():
        kargs["tipo_categoria"] = make_tipo_categoria()
    return mommy.make(Categoria)