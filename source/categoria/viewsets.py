from rest_framework import viewsets

from .models import Categoria, TipoCategoria
from .serializers import CategoriaSerializer, TipoCategoriaSerializer

class TipoCategoriaViewset(viewsets.ModelViewSet):
    queryset = TipoCategoria.objects.all()
    serializer_class = TipoCategoriaSerializer

class CategoriaViewset(viewsets.ModelViewSet):
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerializer
