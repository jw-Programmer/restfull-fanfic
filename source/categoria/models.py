from django.db import models

# Create your models here.
'''Esta classe terá dois modelos. Categoria e Tipo Categoria'''
'''Está é o modelo tipo categoria'''


class TipoCategoria(models.Model):
    nome = models.CharField(max_length=50)

    def __str__(self):
        return self.nome

    class Meta():
        verbose_name = "Tipo de categoria"
        verbose_name_plural = "Tipos de categorias"


class Categoria(models.Model):
    nome = models.CharField(max_length=50)
    tipo = models.ForeignKey(TipoCategoria, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.nome

    class Meta():
        verbose_name = "Categoria"
        verbose_name_plural = "Categorias"
