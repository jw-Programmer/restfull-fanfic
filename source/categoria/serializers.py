from rest_framework import serializers
from .models import Categoria, TipoCategoria

class TipoCategoriaSerializer(serializers.ModelSerializer):
    class  Meta:
        model = TipoCategoria
        fields = '__all__'

class CategoriaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Categoria
        fields = '__all__'